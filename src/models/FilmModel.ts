export class FilmModel {
    picturePath: string;
    description: string;
    releaseDate: string;
    id: number;
    filmName: string;
    filmBackgroundImage: string;

    constructor(
        picturePath: string,
        description: string,
        releaseDate: string,
        id: number,
        filmName: string,
        filmBackgroundImage: string
    ) {
        this.picturePath = picturePath;
        this.description = description;
        this.releaseDate = releaseDate;
        this.id = id;
        this.filmName = filmName;
        this.filmBackgroundImage = filmBackgroundImage;
    }
}
