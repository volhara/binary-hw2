export class GetFilmInfoRequest {
    api_key: string;
    language: string;

    constructor(api_key: string, language: string) {
        this.api_key = api_key;
        this.language = language;
    }
}
