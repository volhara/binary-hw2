export class GetUpcomingFilmsRequest {
    api_key: string;
    language: string;
    page: number;

    constructor(api_key: string, language: string, page: number) {
        this.api_key = api_key;
        this.language = language;
        this.page = page;
    }
}
