export enum ViewTypes {
    Popular,
    Search,
    Upcoming,
    TopRated,
}
