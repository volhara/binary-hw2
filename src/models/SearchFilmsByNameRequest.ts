export class SearchFilmsByNameRequest {
    api_key: string;
    language: string;
    query: string;
    page: number;

    constructor(api_key: string, language: string, query: string, page: number) {
        this.api_key = api_key;
        this.language = language;
        this.query = query;
        this.page = page;
    }
}