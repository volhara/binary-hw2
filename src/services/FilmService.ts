import { FilmModel } from '../models/FilmModel';
import { SearchFilmsByNameRequest } from '../models/SearchFilmsByNameRequest';
import { GetPopularFilmsRequest } from '../models/GetPopularFilmsRequest';
import { GetUpcomingFilmsRequest } from '../models/GetUpcomingFilmsRequest';
import { GetTopRatedFilmsRequest } from '../models/GetTopRatedFilmsRequest';
import { GetFilmInfoRequest } from '../models/GetFilmInfoRequest';
const queryString = require('query-string');

export class FilmService {
    private static baseApiUrl: string = 'https://api.themoviedb.org/3';
    private static basePictureUrl: string =
        'https://www.themoviedb.org/t/p/w220_and_h330_face';
    private static baseBackgroundPictureUrl: string =
        'https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces';
    private static apiKey: string = 'c272c753caee9a45498415a65aa99608';

    public static async getPopularFilms(
        page: number = 1
    ): Promise<Array<FilmModel>> {
        let request: GetPopularFilmsRequest = new GetPopularFilmsRequest(
            this.apiKey,
            'en-US',
            page
        );

        let response = await fetch(
            `${this.baseApiUrl}/movie/popular?${queryString.stringify(request)}`
        );

        let resObj = await response.json();

        let result: Array<FilmModel> = this.MapToFilmArray(resObj);

        return result;
    }

    public static async getUpcomingFilms(
        page: number = 1
    ): Promise<Array<FilmModel>> {
        let request: GetUpcomingFilmsRequest = new GetUpcomingFilmsRequest(
            this.apiKey,
            'en-US',
            page
        );

        let response = await fetch(
            `${this.baseApiUrl}/movie/upcoming?${queryString.stringify(
                request
            )}`
        );

        let resObj = await response.json();

        let result: Array<FilmModel> = this.MapToFilmArray(resObj);

        return result;
    }

    public static async getTopRatedFilms(
        page: number = 1
    ): Promise<Array<FilmModel>> {
        let request: GetTopRatedFilmsRequest = new GetTopRatedFilmsRequest(
            this.apiKey,
            'en-US',
            page
        );

        let response = await fetch(
            `${this.baseApiUrl}/movie/top_rated?${queryString.stringify(
                request
            )}`
        );

        let resObj = await response.json();

        let result: Array<FilmModel> = this.MapToFilmArray(resObj);

        return result;
    }

    public static async searchFilmsByName(
        query: string,
        page: number = 1
    ): Promise<Array<FilmModel>> {
        let request: SearchFilmsByNameRequest = new SearchFilmsByNameRequest(
            this.apiKey,
            'en-US',
            query,
            page
        );

        let response = await fetch(
            `${this.baseApiUrl}/search/movie?${queryString.stringify(request)}`
        );

        let resObj = await response.json();

        let result: Array<FilmModel> = this.MapToFilmArray(resObj);

        return result;
    }

    public static async getFilmInfo(movie_id: number): Promise<FilmModel> {
        let request: GetFilmInfoRequest = new GetFilmInfoRequest(
            this.apiKey,
            'en-US'
        );

        let response = await fetch(
            `${this.baseApiUrl}/movie/${movie_id}?${queryString.stringify(
                request
            )}`
        );

        let resObj = await response.json();

        let result = this.MapToFilmModel(resObj);

        return result;
    }
    private static MapToFilmArray(obj: any): Array<FilmModel> {
        let result: Array<FilmModel> = [];
        for (const iterator of obj.results) {
            result.push(this.MapToFilmModel(iterator));
        }
        return result;
    }
    private static MapToFilmModel(obj: any): FilmModel {
        return new FilmModel(
            `${this.basePictureUrl}${obj.poster_path}`,
            obj.overview,
            obj.release_date,
            obj.id,
            obj.original_title,
            `${this.baseBackgroundPictureUrl}${obj.backdrop_path}`
        );
    }
}
