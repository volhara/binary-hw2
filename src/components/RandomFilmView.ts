import { FilmModel } from '../models/FilmModel';

export class RandomFilmView {
    private randomFilmDiv: HTMLDivElement;
    constructor() {
        this.randomFilmDiv = document.getElementById(
            'random-movie'
        ) as HTMLDivElement;
    }
    public setRandomFilm(films: Array<FilmModel>) {
        let randomNumber = this.getRandomNumber(0, films.length);
        let randomFilm = films[randomNumber];
        this.randomFilmDiv.innerHTML = `<div class="row py-lg-5" style="background-image:url(${randomFilm.filmBackgroundImage})">
        <div
            class="col-lg-6 col-md-8 mx-auto"
            style="background-color: #2525254f"
        >
            <h1 id="random-movie-name" class="fw-light text-light">
                ${randomFilm.filmName}
            </h1>
            <p
                id="random-movie-description"
                class="lead text-white"
            >
                ${randomFilm.description}
            </p>
        </div>
    </div>`;
    }
    private getRandomNumber(start: number, end: number) {
        return Math.floor(Math.random() * (end - start) + start);
    }
}
