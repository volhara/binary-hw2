import { FavoriteTab } from './FavoriteTab';
import { FilmViewer } from './FilmViewer';
import { MenuSelector } from './MenuSelector';

export class App {
    private filmViewer: FilmViewer;
    private menuSelector: MenuSelector;
    private favoriteTab: FavoriteTab;
    constructor() {
        this.favoriteTab = new FavoriteTab();
        this.filmViewer = new FilmViewer(
            this.favoriteTab.onLikeClick.bind(this.favoriteTab)
        );
        this.menuSelector = new MenuSelector(
            this.filmViewer.viewType,
            this.filmViewer.onViewTypeChanged.bind(this.filmViewer)
        );
    }
}
