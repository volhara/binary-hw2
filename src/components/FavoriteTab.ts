import { FilmModel } from '../models/FilmModel';
import { FilmService } from '../services/FilmService';
import { FilmItem } from './FilmItem';

export class FavoriteTab {
    private filmInfoCards: Array<FilmItem> = [];
    private favoriteMoviesDiv: HTMLDivElement;
    constructor() {
        this.favoriteMoviesDiv = document.getElementById(
            'favorite-movies'
        ) as HTMLDivElement;
    }
    public onLikeClick() {
        this.renderFavoriteTab();
    }
    private async renderFavoriteTab() {
        let favoriteFilmIds: Array<number> = [];
        for (let index = 0; index < localStorage.length; index++) {
            favoriteFilmIds.push(parseInt(localStorage.key(index) ?? '-1'));
        }
        let filmInfos: Array<FilmModel> = [];
        for (const id of favoriteFilmIds) {
            filmInfos.push(await FilmService.getFilmInfo(id));
        }
        this.filmInfoCards = [];
        for (const filmInfo of filmInfos) {
            this.filmInfoCards.push(
                new FilmItem(
                    filmInfo,
                    this.onLikeClick.bind(this),
                    'col-12 p-2'
                )
            );
        }
        this.favoriteMoviesDiv.innerHTML = '';
        for (const filmInfoCard of this.filmInfoCards) {
            this.favoriteMoviesDiv.appendChild(filmInfoCard.element);
        }
    }
}
