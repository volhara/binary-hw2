import { ViewTypes } from '../models/enums/ViewTypes';
import { FilmModel } from '../models/FilmModel';
import { FilmService } from '../services/FilmService';
import { FilmItem } from './FilmItem';
import { RandomFilmView } from './RandomFilmView';

export class FilmViewer {
    private filmConteinerDiv: HTMLDivElement;
    private currentPage: number;
    public viewType: ViewTypes;

    private films: Array<FilmItem> = [];
    private lastSearch: string = '';
    private randomFilmView: RandomFilmView;
    private onLikeClick: Function;

    constructor(onLikeclick: Function) {
        this.onLikeClick = onLikeclick;

        this.filmConteinerDiv = document.getElementById(
            'film-container'
        ) as HTMLDivElement;
        this.currentPage = 1;
        this.viewType = ViewTypes.Popular;

        let moreBtn = document.getElementById('load-more') as HTMLButtonElement;
        moreBtn.addEventListener('click', () => {
            this.currentPage++;
            this.showFilmsByType();
        });

        let searchInput = document.getElementById('search') as HTMLInputElement;
        let searchBtn = document.getElementById('submit') as HTMLButtonElement;
        searchBtn.addEventListener('click', () => {
            this.currentPage = 1;
            this.viewType = ViewTypes.Search;
            this.lastSearch = searchInput.value;
            this.filmConteinerDiv.innerHTML = '';
            this.showFilmsByType();
        });

        this.randomFilmView = new RandomFilmView();

        this.showFilmsByType();
    }

    public onViewTypeChanged(viewType: ViewTypes) {
        this.viewType = viewType;
        this.filmConteinerDiv.innerHTML = '';
        this.currentPage = 1;
        this.showFilmsByType();
    }

    private async showFilmsByType() {
        let filmArray: Array<FilmModel> = [];

        switch (this.viewType) {
            case ViewTypes.Popular:
                filmArray = await FilmService.getPopularFilms(this.currentPage);
                break;
            case ViewTypes.Search:
                filmArray = await FilmService.searchFilmsByName(
                    this.lastSearch,
                    this.currentPage
                );
                break;
            case ViewTypes.TopRated:
                filmArray = await FilmService.getTopRatedFilms(
                    this.currentPage
                );
                break;
            case ViewTypes.Upcoming:
                filmArray = await FilmService.getUpcomingFilms(
                    this.currentPage
                );
                break;
            default:
                break;
        }

        this.randomFilmView.setRandomFilm(filmArray);

        this.films = [];
        for (const iterator of filmArray) {
            this.films.push(new FilmItem(iterator, this.onLikeClick));
        }
        for (const iterator of this.films) {
            this.filmConteinerDiv.appendChild(iterator.element);
        }
    }
}
