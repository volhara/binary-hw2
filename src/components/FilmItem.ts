import { Colors } from '../models/enums/Colors';
import { FilmModel } from '../models/FilmModel';

export class FilmItem {
    element: HTMLElement;
    private filmInfo: FilmModel;
    public isLike: boolean = false;
    constructor(
        filmInfo: FilmModel,
        onLikeClick: Function,
        cssClassForCard: string = 'col-lg-3 col-md-4 col-12 p-2'
    ) {
        this.isLike =
            localStorage.getItem(filmInfo.id.toString()) == null ? false : true;
        this.filmInfo = filmInfo;
        this.element = document.createElement('div');
        this.element.className = cssClassForCard;
        this.element.innerHTML = this.getHtmlString();
        this.element.onclick = (e) => {
            if ((e.target as HTMLElement).nodeName == 'path') {
                this.isLike = !this.isLike;
                if (this.isLike) {
                    localStorage.setItem(
                        filmInfo.id.toString(),
                        filmInfo.id.toString()
                    );
                } else {
                    localStorage.removeItem(filmInfo.id.toString());
                }

                let allElements: any = document.getElementsByClassName(
                    filmInfo.id.toString()
                );

                let color: Colors = this.isLike
                    ? Colors.RedFull
                    : Colors.RedTransparent;

                for (let i = 0; i < allElements.length; i++) {
                    let element = allElements[i];
                    element.style.fill = color;
                }
            }
            onLikeClick();
        };
    }
    private getHtmlString(): string {
        let color: Colors = this.isLike
            ? Colors.RedFull
            : Colors.RedTransparent;
        return `<div class="card shadow-sm">
        <img
            src="${this.filmInfo.picturePath}"
        />
        <svg
            xmlns="http://www.w3.org/2000/svg"
            stroke="red"
            fill="${color}"
            width="50"
            height="50"
            class="bi bi-heart-fill position-absolute p-2 ${this.filmInfo.id}"
            viewBox="0 -2 18 22"            
        >
            <path
                fill-rule="evenodd"
                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
            />
        </svg>
        <div class="card-body">
            <p class="card-text truncate">
                ${this.filmInfo.description}
            </p>
            <div
                class="
                    d-flex
                    justify-content-between
                    align-items-center
                "
            >
                <small class="text-muted">${this.filmInfo.releaseDate}</small>
            </div>
        </div>
    </div>`;
    }
}
