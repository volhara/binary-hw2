import { ViewTypes } from '../models/enums/ViewTypes';

export class MenuSelector {
    private viewType: ViewTypes;
    private onChangeViewType: Function;
    constructor(viewType: ViewTypes, onChangeViewType: Function) {
        this.viewType = viewType;
        this.onChangeViewType = onChangeViewType;

        let radioBtns = document.getElementsByName(
            'btnradio'
        ) as NodeListOf<HTMLElement>;

        radioBtns.forEach((element) => {
            element.onclick = this.onClickRadioBtn.bind(this);
        });
    }

    private onClickRadioBtn(e: MouseEvent) {
        switch ((e.target as HTMLElement).id) {
            case 'upcoming':
                this.viewType = ViewTypes.Upcoming;
                break;
            case 'top_rated':
                this.viewType = ViewTypes.TopRated;
                break;
            case 'popular':
                this.viewType = ViewTypes.Popular;
                break;
            default:
                break;
        }
        this.onChangeViewType(this.viewType);
    }
}
